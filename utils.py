import torch

def project_on_boundary(x, bounds, device=torch.device("cuda")):
    """

    :param x:
    :param bounds:
    :return:
    """
    size = x.shape[0]
    dim = x.shape[1]
    lbound, ubound = bounds
    pp = torch.ones(size).to(device) * ubound
    pn = torch.ones(size).to(device) * lbound

    boundary_points = []
    for i in range(1, dim):
        to_append_p = x.clone()
        to_append_p[:, i] = pp

        to_append_n = x.clone()
        to_append_n[:, i] = pn

        boundary_points.append([to_append_p, to_append_n])

    return boundary_points
