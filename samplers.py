from typing import List

import torch


class Sampler:
    def __init__(
        self, dim: int, bounds: List, dtype=torch.cfloat, device=torch.device("cuda")
    ):
        """
        Base class for samaplers
        :param dim:
        :param bounds:
        :param dtype:
        :param device:
        """
        self.dim = dim
        self.tbound, self.lbound, self.ubound = bounds
        self.dtype = dtype
        self.device = device

    def project_on_boundary(self, x):
        """
        Project a batch of points x on the hypercube boundaries
        :param x:
        :param bounds:
        :return:
        """
        size = x.shape[0]
        dim = x.shape[1]
        pp = torch.ones(size).to(self.device) * self.ubound
        pn = torch.ones(size).to(self.device) * self.lbound

        boundary_points = []
        for i in range(1, dim):
            to_append_p = x.clone()
            to_append_p[:, i] = pp

            to_append_n = x.clone()
            to_append_n[:, i] = pn

            boundary_points.append(torch.cat((to_append_p, to_append_n)))

        boundary_points = torch.cat(boundary_points)

        return boundary_points


class UniformSampler(Sampler):
    def __init__(
        self, dim: int, bounds: List, dtype=torch.cfloat, device=torch.device("cuda")
    ):
        """
        Class to uniformly from a hypercube
        :param dim:
        :param bounds:
        :param dtype:
        :param device:
        """
        super().__init__(dim, bounds, dtype, device)

    def sample(self, batch_size: int):
        """
        Sample uniformly from a hypercube
        :param batch_size:
        :return:
        """
        t = torch.Tensor(batch_size, 1).to(self.device).uniform_(0, self.tbound)
        points = (
            torch.Tensor(batch_size, self.dim)
            .to(self.device)
            .uniform_(self.lbound, self.ubound)
            .type(self.dtype)
        )
        points = torch.cat((t, points), axis=1)
        return points
