import torch


def initial_function_1D(x):
    """
    Initial conditions for a 1D system corresponding to a particle with momentum to the left.
    :param x:
    :return:
    """
    x = torch.real(x)

    ic_real = torch.exp(-((x[:, 1] - 1) ** 2)) * torch.cos(-x[:, 1])
    ic_im = torch.exp(-((x[:, 1] - 1) ** 2)) * torch.sin(-x[:, 1])

    ic = torch.cat((ic_real.view(-1, 1), ic_im.view(-1, 1)), axis=1)
    ic = torch.view_as_complex(ic).view(-1, 1)
    return ic
