import time
from typing import Callable, Dict

import torch
from torch.utils.tensorboard import SummaryWriter

from conditions import initial_function_1D
from models import FFFC
from samplers import UniformSampler
from schrodinger import TDSE


def train(equation: TDSE, sampler: Callable, config: Dict):
    """
    Training pipeline
    :param equation:
    :param sampler:
    :param config:
    :return:
    """

    lr = config["lr"]
    epochs = config["epochs"]
    batch_size = config["batch_size"]
    print_every = config["print_every"]

    writer = SummaryWriter()
    log_dir = writer.log_dir

    model = equation.model

    model.train()
    params = list(model.parameters())

    total_params = sum(p.numel() for p in params)

    print(
        "{} trainable tensors, totaling {} trainable parameters.".format(
            len(params), total_params
        )
    )

    optimizer = torch.optim.Adam(
        params, lr=lr, betas=(0.9, 0.999), eps=1e-08, weight_decay=0, amsgrad=False
    )

    scheduler = torch.optim.lr_scheduler.StepLR(
        optimizer, step_size=int(epochs / config["frequency"]), gamma=config["gamma"]
    )

    points = sampler(batch_size)
    writer.add_graph(model, points)
    print("Training!")
    for i in range(epochs):
        tic = time.time()

        model.zero_grad()

        points = sampler(batch_size)
        points.requires_grad = True
        loss = equation.loss(points)
        if not torch.isfinite(loss):
            break
        loss.backward()
        optimizer.step()

        tac = time.time()
        epoch_time = tac - tic

        scheduler.step()

        ### Logs
        writer.add_scalar("Loss", equation.record_loss, i)
        writer.add_scalar("Structural loss", equation.record_structural, i)
        writer.add_scalar("Boundary loss", equation.record_boundary, i)
        writer.add_scalar("Initial loss", equation.record_initial, i)

        if i % print_every == 0 or i == epochs - 1:

            print("{}: {:1.5e}, time: {:1.2f}s".format(i, loss.item(), epoch_time))
            torch.save(model.state_dict(), log_dir + "/model.pt")

    writer.close()


if __name__ == "__main__":

    config = {
        "dim": 1,
        "lr": 1e-3,
        "epochs": int(5000),
        "batch_size": int(1000),
        "frequency": 5,
        "gamma": 0.66,
        "bounds": [1.0, -1.0, 1.0],
        "print_every": 100,
    }

    fffc = FFFC(config["dim"] + 1, [20, 20, 20])
    us = UniformSampler(config["dim"], config["bounds"])
    sample = us.sample
    tdse = TDSE(
        fffc,
        potential=lambda x: 0,
        initial=initial_function_1D,
        boundary=lambda x: 0,
        boundary_sampler=us.project_on_boundary,
    )

    train(tdse, sample, config)
