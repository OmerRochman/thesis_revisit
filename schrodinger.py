from typing import Callable

import torch

from samplers import Sampler


class TDSE:
    def __init__(
        self,
        model: torch.nn.Module,
        potential: Callable,
        initial: Callable,
        boundary: Callable,
        boundary_sampler: Sampler,
    ):
        """
        Class to implement the Time Dependent Schrodinger Equation
        :param model:
        :param initial:
        :param boundary:
        """
        self.model = model
        self.initial = initial
        self.boundary = boundary
        self.potential = potential
        self.boundary_sampler = boundary_sampler

        self.h = 1
        self.M = 1

    def compute_structural_loss(self, x):
        """
        Implement the structural loss based on the equation and compute the necessary derivatives
        i * h * u_t = h**2 / 2M * u_xx + V * u
        :param x:
        :return:
        """

        V = self.potential(x)
        u = self.model(x)
        ut = torch.autograd.grad(u.sum(), x, create_graph=True, only_inputs=True)[0]
        dt = ut[:, 0].view(-1, 1)
        uxx = torch.autograd.grad(
            ut[:, 1].sum(), x, create_graph=True, only_inputs=True
        )[0]
        ham = uxx[:, 1].view(-1, 1)

        f = self.h * 1j * dt + (self.h ** 2 / (2 * self.M)) * ham  # - V * u
        f = torch.abs(f).mean()

        return f

    def compute_initial_loss(self, x):
        """
        MSE between model and initial conditions at points x[:, 0] = 0 i.e. t = 0
        :param x:
        :return:
        """
        points = x.clone().detach()
        points[:, 0] = 0

        u = self.model(points)
        l = self.initial(points) - u
        l = torch.abs(l).mean()

        return l

    def compute_boundary_loss(self, x):
        """
        MSE between model and boundary conditions at points on the boundaries
        :param x:
        :return:
        """
        points = x.clone().detach()
        points = self.boundary_sampler(points)

        u = self.model(points)
        l = torch.add(self.boundary(points), -u)
        l = torch.abs(l).mean()
        return l

    def loss(self, x):
        """
        Composite loss (sum) and hacky logging
        :param x:
        :return:
        """
        structural = self.compute_structural_loss(x)
        initial = self.compute_initial_loss(x)
        boundary = self.compute_boundary_loss(x)
        loss = initial + structural

        # Hacky logs
        self.record_structural = structural.item()
        self.record_initial = initial.item()
        self.record_boundary = boundary.item()
        self.record_loss = loss.item()

        return loss
