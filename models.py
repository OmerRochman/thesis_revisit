from typing import List

import torch


class TanhLayer(torch.nn.Module):
    """
    Wrapper of Linear + Activation into one nn.Module
    """

    def __init__(self, dim: int, dim_out: int, activation=torch.nn.Tanh):
        super().__init__()
        self.linear = torch.nn.Linear(dim, dim_out)
        self.activation = activation()

    def forward(self, x):
        """

        :param x:
        :return:
        """
        out = self.linear(x)
        out = self.activation(out)
        return out


class FFFC(torch.nn.Module):
    def __init__(self, dim: int, architecture: List):
        """
        Create a Feed Forward Fully Connected NN
        :param dim:
        :param architecture:
        """
        super().__init__()
        self.layers = self._build_FFFC(dim, architecture)
        self.to("cuda", torch.cfloat)

    def _build_FFFC(self, dim, architecture):
        """
        Build the model with an arbitrary list of layers and neurons: [10, 10 10] = 3 layers, 10 neurons each
        :param dim:
        :param architecture:
        :return:
        """
        l = torch.nn.ModuleDict()

        name = "input_layer"
        l[name] = TanhLayer(dim, architecture[0])

        for i in range(len(architecture) - 1):
            name = "layer_" + str(i)
            l[name] = TanhLayer(architecture[i], architecture[i + 1])

        name = "output_layer"
        l[name] = torch.nn.Linear(architecture[-1], 1)

        return l

    def forward(self, x):
        """

        :param x:
        :return:
        """

        for i, layer in self.layers.items():
            if i == "output_layer":
                break
            x = layer(x)

        x = self.layers["output_layer"](x)
        return x
